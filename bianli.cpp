#include <iostream>
#include <stack>
using namespace std;

typedef struct BinTreeNode{
	void * data;
	struct BinTreeNode *left ;
	struct BinTreeNode *right ;
}BinTreeNode_t;

void preOrder(BinTreeNode_t *root)
{
	stack<BinTreeNode_t *> s ;
	BinTreeNode_t *tmp = root ;
	while(tmp != NULL || !s.empty())
	{
		while(tmp != NULL)
		{
			cout<<*(tmp->data)<< " ";
			s.push(tmp);
			tmp = tmp->left;
		}
		if(!s.empty())
		{
			tmp = s.top()->right ;
			s.pop();
		}
	}
}

void minOrder(BinTreeNode_t *root)
{
	stack<BinTreeNode_t *> s;
	BinTreeNode_t *tmp = root ;
	while(tmp!= NULL || !s.empty())
	{
		while(tmp!=NULL)
		{
			s.push(tmp);
			tmp = tmp->left ;
		}
		if(!s.empty())
		{
			cout<<*(tmp->data)<<" ";
			tmp = s.top()->right;
			s.pop() ;
		}
	}
}
